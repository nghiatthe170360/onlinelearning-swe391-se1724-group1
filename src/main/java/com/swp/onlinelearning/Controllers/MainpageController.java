package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class MainpageController {
    @Autowired
    final SubjectService service;

    //    public MainpageController(SubjectService service) {
    //        this.service = service;
    //    }

    @GetMapping("/")
    public String getMainpage(Model model) {
        List<Subjects> ListSubjects = service.ListAll();
        model.addAttribute("ListSubjects", ListSubjects);
        return "mainpage";
    }
    @RequestMapping("/createSubject")
    public String showNewSubjectPage(Model model){
        Subjects subject = new Subjects();
        model.addAttribute("subject", subject);
        return "createSubject";
    }
    @RequestMapping(value = "/saveSubject",method = RequestMethod.POST)
    public String saveSubject(@ModelAttribute("categories") Subjects subject){
        service.save(subject);
        return "redirect:/";
    }
    @RequestMapping("/editSubject/{id}")
    public ModelAndView showEditCategoriesPage(@PathVariable(name = "id") int id){
        ModelAndView mav = new ModelAndView("editSubject");
        Subjects subject = service.get(id);
        mav.addObject("subject", subject);
        return mav;
    }
    @RequestMapping("/deleteSubject{id}")
    public String deleteSubject(@PathVariable(name = "id") int id){
        service.delete(id);
        return "redirect:/";
    }
}
