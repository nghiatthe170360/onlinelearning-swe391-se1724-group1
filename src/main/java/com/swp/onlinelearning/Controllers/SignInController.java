package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class SignInController {
    @Autowired
    private UserService userService;

    @GetMapping("/signin")
    public String showLoginForm() {
        return "signin";
    }

    @PostMapping("/signin-submit")
    public String authenticateUser(@RequestParam("email") String email, @RequestParam("password") String password, Model model) {

        UserDetails userDetails = userService.loadUserByUsername(email);
        if (userDetails != null && userDetails.getPassword().equals(password)) {
            model.addAttribute("user", userDetails);
            return "mainpage";
        } else {
            model.addAttribute("error", "Invalid email or password");
            return "signin";
        }
    }
}

