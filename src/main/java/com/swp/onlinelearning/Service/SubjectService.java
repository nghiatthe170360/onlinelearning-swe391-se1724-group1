package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Repository.SubjectRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepo;
    public List<Subjects> ListAll(){
        return subjectRepo.findAll();
    }

    public void save(Subjects subject){
        subjectRepo.save(subject);
    }

    public Subjects get(int id){
        if (subjectRepo.findById(id).isPresent()) {
            return subjectRepo.findById(id).get();
        } else {
            return null;
        }
    }

    public void delete(int id){
        subjectRepo.deleteById(id);
    }
}
