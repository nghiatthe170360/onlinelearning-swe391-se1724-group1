package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.SubjectCategories;
import com.swp.onlinelearning.Repository.CategoryRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepo;
    public List<SubjectCategories> ListAll(){
        return categoryRepo.findAll();
    }

    public void save(SubjectCategories categories){
        categoryRepo.save(categories);
    }

    public SubjectCategories get(int id){
        if (categoryRepo.findById(id).isPresent()) {
            return categoryRepo.findById(id).get();
        } else {
            return null;
        }
    }

    public void delete(int id){
        categoryRepo.deleteById(id);
    }
}
