package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "registrations")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Registrations implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "UserID", nullable = false)
    private Integer userId;

    @Column(name = "SubjectID", nullable = false)
    private Integer subjectId;

    @Column(name = "PackageID", nullable = false)
    private Integer packageId;

    @Column(name = "TotalCost", nullable = false)
    private BigDecimal totalCost;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;
    @Temporal(TemporalType.DATE)
    @Column(name = "ValidFrom")
    private Date validFrom;
    @Temporal(TemporalType.DATE)
    @Column(name = "ValidTo")
    private Date validTo;

    public enum Status {
        Submitted, Paid, Cancelled
    }

}
