package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "subjects")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subjects implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "CategoryID")
    private Integer categoryId;

    @Column(name = "ThumbnailImage")
    private String thumbnailImage;

    @Column(name = "OwnerID")
    private Integer ownerId;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;
    @Column(name = "Dimension")
    private String dimension;
    @Column(name = "Description")
    private String description;
    @Column(name = "Duration")
    private Integer duration;
    @Temporal(TemporalType.DATE)
    @Column(name = "DateCreated", nullable = false)
    private Date dateCreated;

    public enum Status {
        Draft, Published, Unpublished
    }

}