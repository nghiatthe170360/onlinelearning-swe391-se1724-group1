package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "pricepackages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PricePackages implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "SubjectID", nullable = false)
    private Integer subjectId;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "AccessDuration", nullable = false)
    private Integer accessDuration;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;
    @Column(name = "ListPrice", nullable = false)
    private BigDecimal listPrice;
    @Column(name = "SalePrice")
    private BigDecimal salePrice;
    @Column(name = "Description")
    private String description;

    public enum Status {
        Active, Inactive
    }

}
