package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.SubjectCategories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<SubjectCategories, Integer> {
}

