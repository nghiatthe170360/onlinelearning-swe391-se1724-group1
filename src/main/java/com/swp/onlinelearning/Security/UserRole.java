package com.swp.onlinelearning.Security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public enum UserRole {
    COMMON(newHashSet(UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.SUBJECT_VIEW, UserPermission.PRICEPACKAGE_VIEW)), CUSTOMER(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTCATEGORY_VIEW, UserPermission.SLIDE_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW)), LEARNING(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTCATEGORY_VIEW, UserPermission.SLIDE_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW)), MARKETING(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTCATEGORY_VIEW, UserPermission.SLIDE_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW)), COURSE_CONTENT_CREATOR(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_CREATE, UserPermission.COURSE_UPDATE, UserPermission.COURSE_DELETE, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_CREATE, UserPermission.SUBJECT_UPDATE, UserPermission.SUBJECT_DELETE, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_CREATE, UserPermission.SUBJECT_CATEGORY_UPDATE, UserPermission.SUBJECT_CATEGORY_DELETE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTS_CREATE, UserPermission.POSTCATEGORY_VIEW, UserPermission.POSTCATEGORY_CREATE, UserPermission.SLIDE_VIEW, UserPermission.SLIDE_CREATE, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.LESSON_CREATE, UserPermission.LESSON_UPDATE, UserPermission.LESSON_DELETE, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW)), TEST_CONTENT_CREATOR(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTCATEGORY_VIEW, UserPermission.SLIDE_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUIZ_CREATE, UserPermission.QUIZ_UPDATE, UserPermission.QUIZ_DELETE, UserPermission.QUESTION_VIEW, UserPermission.QUESTION_CREATE, UserPermission.QUESTION_UPDATE, UserPermission.QUESTION_DELETE, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.ANSWEROPTIONS_CREATE, UserPermission.ANSWEROPTIONS_UPDATE, UserPermission.ANSWEROPTIONS_DELETE, UserPermission.QUIZQUESTION_CREATE, UserPermission.QUIZQUESTION_UPDATE, UserPermission.QUIZQUESTION_DELETE, UserPermission.QUIZQUESTION_VIEW)), SALE(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.POSTS_VIEW, UserPermission.POSTCATEGORY_VIEW, UserPermission.SLIDE_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW, UserPermission.PRICEPACKAGE_CREATE, UserPermission.PRICEPACKAGE_UPDATE, UserPermission.PRICEPACKAGE_DELETE, UserPermission.PRICEPACKAGE_VIEW)), ADMIN(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_WRITE, UserPermission.USER_PROFILE_UPDATE, UserPermission.USER_PROFILE_DELETE, UserPermission.COURSE_REGISTER, UserPermission.COURSE_VIEW, UserPermission.COURSE_CREATE, UserPermission.COURSE_UPDATE, UserPermission.COURSE_DELETE, UserPermission.LESSON_VIEW, UserPermission.LESSON_CREATE, UserPermission.LESSON_UPDATE, UserPermission.LESSON_DELETE, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CREATE, UserPermission.SUBJECT_UPDATE, UserPermission.SUBJECT_DELETE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.SUBJECT_CATEGORY_CREATE, UserPermission.SUBJECT_CATEGORY_UPDATE, UserPermission.SUBJECT_CATEGORY_DELETE, UserPermission.POSTS_VIEW, UserPermission.POSTS_CREATE, UserPermission.POSTS_UPDATE, UserPermission.POSTS_DELETE, UserPermission.POSTCATEGORY_CREATE, UserPermission.POSTCATEGORY_VIEW, UserPermission.POSTCATEGORY_UPDATE, UserPermission.POSTCATEGORY_DELETE, UserPermission.PRICEPACKAGE_VIEW, UserPermission.PRICEPACKAGE_CREATE, UserPermission.PRICEPACKAGE_UPDATE, UserPermission.PRICEPACKAGE_DELETE, UserPermission.QUIZ_CREATE, UserPermission.QUIZ_VIEW, UserPermission.QUIZ_UPDATE, UserPermission.QUIZ_DELETE, UserPermission.QUESTION_VIEW, UserPermission.QUESTION_CREATE, UserPermission.QUESTION_UPDATE, UserPermission.QUESTION_DELETE, UserPermission.QUIZQUESTION_CREATE, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZQUESTION_UPDATE, UserPermission.QUIZQUESTION_DELETE, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.ANSWEROPTIONS_CREATE, UserPermission.ANSWEROPTIONS_UPDATE, UserPermission.ANSWEROPTIONS_DELETE, UserPermission.REGISTRATION_CREATE, UserPermission.REGISTRATION_VIEW, UserPermission.REGISTRATION_UPDATE, UserPermission.REGISTRATION_DELETE, UserPermission.SLIDE_CREATE, UserPermission.SLIDE_VIEW, UserPermission.SLIDE_UPDATE, UserPermission.SLIDE_DELETE));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    @SafeVarargs
    public static <T> Set<T> newHashSet(T... objs) {
        Set<T> set = new HashSet<T>();
        Collections.addAll(set, objs);
        return set;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream().map(permission -> new SimpleGrantedAuthority(permission.getPermission())).collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
