package com.swp.onlinelearning.Security;

import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final SecurityServices securityServices;
    private final UserService userService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeHttpRequests(requests -> {
            try {
                requests.requestMatchers("/admin/**").hasAuthority("Admin").requestMatchers("/customer/**").hasAuthority("Customer").requestMatchers("/learning/**").hasAuthority("Learning").requestMatchers("/marketing/**").hasAuthority("Marketing").requestMatchers("/course-content-creator/**").hasAuthority("CourseContentCreator").requestMatchers("/test-content-creator/**").hasAuthority("TestContentCreator").requestMatchers("/sale/**").hasAuthority("Sale").requestMatchers("/**", "/css/**", "/img/**", "/signin").permitAll().anyRequest().authenticated();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        http.formLogin().loginPage("/signin").loginProcessingUrl("/signin-submit").permitAll().defaultSuccessUrl("/mainpage", true).passwordParameter("password").usernameParameter("email").and().rememberMe().tokenValiditySeconds((int)TimeUnit.SECONDS.toSeconds(20)).key("somethingverysecured").rememberMeParameter("remember-me").and().logout().logoutUrl("/signout").logoutRequestMatcher(new AntPathRequestMatcher("/signout", "GET")); // https://docs.spring.io/spring-security/site/docs/4.2.12.RELEASE/apidocs/org/springframework/security/config/annotation/web/configurers/LogoutConfigurer.html.clearAuthentication(true).invalidateHttpSession(true).deleteCookies("JSESSIONID", "remember-me").logoutSuccessUrl("/login");

        http.authenticationProvider(daoAuthenticationProvider());

        return http.build();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(securityServices.passwordEncoder());
        provider.setUserDetailsService(userService);
        return provider;
    }
}