<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/mainPage.css">
        <script>
            function myFunction(x) {
                x.classList.toggle("fa-heartbeat");
                document.getElementById("heartStyle").classList.toggle("heartStyle");
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="margin-top: 30px; display: flex">
                <div class="col-lg-3 col-md-5">
                    <div class="left">
                        <div class="user_button">
                            <a href="#" class="next round">
                                <div class="item"></div>
                                <b style="font-size: 10px; color: white; margin-left: 5px; margin-top: 15px">
                                    <strong style="font-size: 17px">Lai Khanh Trang</strong>
                                    <br/>TrangLK123456@fpt.edu.vn
                                </b>
                                <div style="margin-top: 3px; margin-left: 45px">
                                    <i class="glyphicon glyphicon-menu-right" style="color: white; font-size: 20px"></i>
                                </div>
                            </a>
                        </div>
                        <div class="home_button">
                            <a href="#" class="home round">
                                <i class="glyphicon glyphicon-home" style="color: white; margin-top: 8px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px">
                                    <b style="color: white; font-size: 15px">Home</b>
                                </div>
                            </a>
                        </div>
                        <div class="button_left">
                            <a href="#" class="button round">
                                <i class="glyphicon glyphicon-bookmark" style= "margin-top: 10px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px">
                                    <b style="font-size: 15px">Course</b>
                                </div>
                            </a>
                        </div>
                        <div class="button_left">
                            <a href="#" class="button round">
                                <i class="glyphicon glyphicon-user" style= "margin-top: 10px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px">
                                    <b style="font-size: 15px">Profile</b>
                                </div>
                            </a>
                        </div>
                        <div class="button_left">
                            <a href="#" class="button round">
                                <i class="glyphicon glyphicon-cog" style= "margin-top: 10px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px">
                                    <b style="font-size: 15px">Setting</b>
                                </div>
                            </a>
                        </div>
                        <div class="button_left">
                            <a href="#" class="button round">
                                <i class="glyphicon glyphicon-info-sign" style= "margin-top: 10px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px">
                                    <b style="font-size: 15px">More</b>
                                </div>
                            </a>
                        </div>
                        <div class="button_left" style="margin-bottom: 10px">
                            <a href="#" class="button round">
                                <i class="glyphicon glyphicon-log-out" style= "margin-top: 10px; margin-left: 10px"></i>
                                <div style="margin-top: 6px; margin-left: 15px;">
                                    <b style="font-size: 15px">Logout</b>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="center" style="height: 500px; width: 900px">
                        <div class="user_info">
                            <div class="avatar"></div>
                            <div class="greeting" >
                                <b>Welcome,</b><br/>
                                <h4 style="color: black"><strong>Lai Khanh Trang</strong></h4>
                            </div>
                        </div>
                        <div class="center_content">
                            <div style="display: flex">
                                <i class="fa fa-bookmark-o" style= "font-size: 20px; margin-top: 5px"></i>
                                <p><strong style="margin-left: 10px; font-size: 20px">Courses</strong></p>
                            </div>
                            <div class="row">
                                <a href="#" class="box one">
                                    <div style="height: 35px; width: 35px; background-color: orange; border-radius: 50%; margin-top: 2px; margin-left: 4px; margin-bottom: 2px">
                                        <i class="glyphicon glyphicon-book" style= "color: white; font-size: 20px; margin-top: 6px; margin-left: 6px"></i>
                                    </div>
                                    <p style="margin-top: 9px"><strong style="margin-left: 5px; margin-right: 20px">New</strong></p>
                                </a>
                                <a href="#" class="box two">
                                    <div style="height: 35px; width: 35px; background-color: #44c144; border-radius: 50%; margin-top: 2px; margin-left: 4px; margin-bottom: 2px">
                                        <i class="glyphicon glyphicon-book" style= "color: white; font-size: 20px; margin-top: 6px; margin-left: 6px"></i>
                                    </div>
                                    <p style="margin-top: 9px"><strong style="margin-left: 5px; margin-right: 20px">Free</strong></p>
                                </a>
                                <a href="#" class="box three">
                                    <div style="height: 35px; width: 35px; background-color: blue; border-radius: 50%; margin-top: 2px; margin-left: 4px; margin-bottom: 2px">
                                        <i class="glyphicon glyphicon-book" style= "color: white; font-size: 20px; margin-top: 6px; margin-left: 6px"></i>
                                    </div>
                                    <p style="margin-top: 9px"><strong style="margin-left: 5px; margin-right: 20px">Popular</strong></p>
                                </a>
                                <a href="#" class="box four">
                                    <div style="height: 35px; width: 35px; background-color: blueviolet; border-radius: 50%; margin-top: 2px; margin-left: 4px; margin-bottom: 2px">
                                        <i class="glyphicon glyphicon-book" style= "color: white; font-size: 20px; margin-top: 6px; margin-left: 6px"></i>
                                    </div>
                                    <p style="margin-top: 9px"><strong style="margin-left: 5px; margin-right: 20px">Paid</strong></p>
                                </a>
                                <a href="#" class="box five">
                                    <div style="height: 35px; width: 35px; background-color: red; border-radius: 50%; margin-top: 2px; margin-left: 4px; margin-bottom: 2px">
                                        <i class="glyphicon glyphicon-book" style= "color: white; font-size: 20px; margin-top: 6px; margin-left: 6px"></i>
                                    </div>
                                    <p style="margin-top: 9px"><strong style="margin-left: 5px; margin-right: 20px">Upcoming</strong></p>
                                </a>
                            </div>
                            <div class="row">
                                <a href="#" class="box_course">
                                    <div class="course img"></div>
                                    <div class="mini_item">
                                        <p style="margin-left: 10px"><i class="glyphicon glyphicon-education"></i></p>
                                        <p style="margin-left: 5px"><strong>700 students</strong></p>
                                        <p style="margin-left: 90px"><i class="glyphicon glyphicon-time"></i></p>
                                        <p style="margin-left: 5px"><strong>2h 20m</strong></p>
                                    </div>
                                    <div class="name_item">
                                        <h3 style="margin-top: 0px; margin-left: 15px">
                                            <strong>Mastering HTML/CSS With Bootstrap</strong>
                                        </h3>
                                    </div>
                                    <div class="heart" style="font-size: 17px; display: flex">
                                        <div class="mini_avatar"></div>
                                        <p style="margin-top: 8px; margin-left: 5px; font-size: 12px"><strong>Lai Khanh Trang</strong></p>
                                        <div id="heartStyle">
                                            <i onclick="myFunction(this)" class="fa fa-heart-o" style="margin-top: 6px; margin-left: 90px"></i>
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="box_course">
                                    <div class="course img"></div>
                                    <div class="mini_item">
                                        <p style="margin-left: 10px"><i class="glyphicon glyphicon-education"></i></p>
                                        <p style="margin-left: 5px"><strong>700 students</strong></p>
                                        <p style="margin-left: 90px"><i class="glyphicon glyphicon-time"></i></p>
                                        <p style="margin-left: 5px"><strong>2h 20m</strong></p>
                                    </div>
                                    <div class="name_item">
                                        <h3 style="margin-top: 0px; margin-left: 15px">
                                            <strong>Mastering HTML/CSS With Bootstrap</strong>
                                        </h3>
                                    </div>
                                    <div class="heart" style="font-size: 17px; display: flex">
                                        <div class="mini_avatar"></div>
                                        <p style="margin-top: 8px; margin-left: 5px; font-size: 12px"><strong>Lai Khanh Trang</strong></p>
                                        <div id="heartStyle">
                                            <i onclick="myFunction(this)" class="fa fa-heart-o" style="margin-top: 6px; margin-left: 90px"></i>
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="box_course">
                                    <div class="course img"></div>
                                    <div class="mini_item">
                                        <p style="margin-left: 10px"><i class="glyphicon glyphicon-education"></i></p>
                                        <p style="margin-left: 5px"><strong>700 students</strong></p>
                                        <p style="margin-left: 90px"><i class="glyphicon glyphicon-time"></i></p>
                                        <p style="margin-left: 5px"><strong>2h 20m</strong></p>
                                    </div>
                                    <div class="name_item">
                                        <h3 style="margin-top: 0px; margin-left: 15px">
                                            <strong>Mastering HTML/CSS With Bootstrap</strong>
                                        </h3>
                                    </div>
                                    <div class="heart" style="font-size: 17px; display: flex">
                                        <div class="mini_avatar"></div>
                                        <p style="margin-top: 8px; margin-left: 5px; font-size: 12px"><strong>Lai Khanh Trang</strong></p>
                                        <div id="heartStyle">
                                            <i onclick="myFunction(this)" class="fa fa-heart-o" style="margin-top: 6px; margin-left: 90px"></i>
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="box_course">
                                    <div class="course img"></div>
                                    <div class="mini_item">
                                        <p style="margin-left: 10px"><i class="glyphicon glyphicon-education"></i></p>
                                        <p style="margin-left: 5px"><strong>700 students</strong></p>
                                        <p style="margin-left: 90px"><i class="glyphicon glyphicon-time"></i></p>
                                        <p style="margin-left: 5px"><strong>2h 20m</strong></p>
                                    </div>
                                    <div class="name_item">
                                        <h3 style="margin-top: 0px; margin-left: 15px">
                                            <strong>Mastering HTML/CSS With Bootstrap</strong>
                                        </h3>
                                    </div>
                                    <div class="heart" style="font-size: 17px; display: flex">
                                        <div class="mini_avatar"></div>
                                        <p style="margin-top: 8px; margin-left: 5px; font-size: 12px"><strong>Lai Khanh Trang</strong></p>
                                        <div id="heartStyle">
                                            <i onclick="myFunction(this)" class="fa fa-heart-o" style="margin-top: 6px; margin-left: 90px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2">
                    <div class="bar">
                        <div class="dropdown">
                            <button class="dropbtn"><i class="glyphicon glyphicon-cog"></i></button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn"><i class="glyphicon glyphicon-bell"></i></button>
                            <div class="dropdown-content">
                                <p>Notification 1</p>
                                <p>Notification 2</p>
                                <p>Notification 3</p>
                            </div>
                        </div>
                        <form action="" id="search-box">
                            <button id="search-button"><i class="glyphicon glyphicon-search" style="color: gray"></i></button>
                            <input type="text" id="search-text" placeholder="Search" required="">
                        </form>
                    </div>
                    <div class="right">
                        <div class="right_content">
                            <i class="fa fa-bookmark-o" style= "font-size: 20px; margin-top: 7px"></i>
                            <p><strong style="margin-left: 10px; font-size: 22px">Upcoming Courses</strong></p>
                        </div>
                        <a href="#" class="right_box" style="display: flex; margin-bottom: 50px">
                            <div class="col-md-5 box_img"></div>
                            <div class="col-md-7">
                                <h4 style="margin-top: 10px; margin-bottom: 2px">
                                    <strong>Mastering HTML/CSS With Bootstrap</strong>
                                </h4>
                                <p><strong>2h 20m</strong></p>                            
                            </div>
                        </a>
                        <div class="right_content">
                            <i class="fa fa-star-o" style= "font-size: 20px; margin-top: 7px"></i>
                            <p style="margin-top: 2px"><strong style="margin-left: 10px; font-size: 22px">Favorite Courses</strong></p>
                        </div>
                        <a href="#"  class="right_box" style="display: flex; margin-bottom: 20px">
                            <div class="col-md-5 box_img"></div>
                            <div class="col-md-7">
                                <h4 style="margin-top: 10px; margin-bottom: 2px">
                                    <strong>Mastering HTML/CSS With Bootstrap</strong>
                                </h4>
                                <p><strong>2h 20m</strong></p>                            
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
