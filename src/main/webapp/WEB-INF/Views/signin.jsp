<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sign In</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/signin-style.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,700&display=swap" rel="stylesheet">
</head>
<body>
<header>
  <div class="header">
    <div class="navlink">
      <ul class="icon"><i class="fas fa-bars"></i></ul>
      <ul><a href="#"><img src="logo.png" alt="logo"></a></ul>
      <ul class="icon"><i class="fas fa-user"></i></ul>
      <ul class="list"><a href="#">Products</a></ul>
      <ul class="list"><a href="#">Music</a></ul>
      <ul class="list"><a href="#">Support</a></ul>
      <ul class="list"><a href="#" id="active">Sign in</a></ul>
      <ul class="list"><a href="#"><i class="fas fa-search"></i></a></ul>
      <ul class="list"><a href="#"><i class="fas fa-shopping-bag"></i></a></ul>
    </div>
  </div>
</header>
<main>
  <div class="signinbox">
    <form class="form">
      <h1 class="signintext">Sign in.</h1>
      <input type="text" class="email" placeholder="Email" name="email" id="email" required>
      <input type="password" class="password" placeholder="Password" name="password" id="password" required>
      <input type="submit" class="button" value="Sign in">
      <a href="#" class="link">Forgot your password?</a>
      <a href="#" class="link">Don't have an account? Create one now.</a>
    </form>
  </div>
</main>
</body>
</html>