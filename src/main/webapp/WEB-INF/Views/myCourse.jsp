<%--
  Created by IntelliJ IDEA.
  User: Lan
  Date: 19/05/2023
  Time: 8:46 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
            integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
    />
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
            crossorigin="anonymous"
    />
</head>
<body>
<div class="container">

    <div class="card rounded-1 border border-2 p-3">
        <div class="row">
            <div class="col-lg-1 col-3 d-flex align-items-center">
                <img
                        src="https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://coursera-course-photos.s3.amazonaws.com/33/56fa78ec0542f19eaa977b8854c833/coursera2.jpg?auto=format%2Ccompress&dpr=1"
                        class="w-100 h-auto"
                        alt=""
                />
            </div>
            <div class="col-lg-9 col-6 d-flex align-items-center flex-column justify-content-between">
                <div class="w-100">
                    <div class="d-flex align-items-center justify-content-between">
                        <h5 class="card-title">Foundations of Project Management</h5>
                        <div class="btn-group">
                            <button
                                    type="button"
                                    class="btn bg-transparent"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false"
                            >
                                <i class="fa-solid fa-ellipsis"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li>
                                    <a class="dropdown-item" href="#">Unenroll from Course</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <span>Google</span>
                </div>
                <div class="w-100">
                    <div class="fw-bold">Progress:0%</div>
                    <div
                            class="progress"
                            role="progressbar"
                            aria-label="Basic example"
                            aria-valuenow="0"
                            aria-valuemin="0"
                            aria-valuemax="100"
                    >
                        <div class="progress-bar" style="width: 0%"></div>
                    </div>
                    <div class="fw-bold">1 assignment this week</div>
                </div>
            </div>
            <div class="col-2 my-auto">
                <div class="fw-bold">Week 1 of 4</div>
                <button type="button" class="btn btn-primary">Resume</button>
            </div>
        </div>
    </div>
</div>

<script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.7/dist/umd/popper.min.js"
        integrity="sha384-zYPOMqeu1DAVkHiLqWBUTcbYfZ8osu1Nd6Z89ify25QV9guujx43ITvfi12/QExE"
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.min.js"
        integrity="sha384-Y4oOpwW3duJdCWv5ly8SCFYWqFDsfob/3GkgExXKV4idmbt98QcxXYs9UoXAB7BZ"
        crossorigin="anonymous"
></script>
</body>
</html>
